/* Includes */
#include "stm32f4xx.h"
#include "BSP_LED.h"
#include "BSP_PWR.h"
#include "BSP_systick.h"
#include "BSP_IC.h"
#include "stm32f4xx_tim.h"
#include "BSP_ADC.h"

/* ENUMS */
enum 
{
	NBR_OF_CHANNELS = 2,
	SAMPLING_FREQUENCY = 100,
	OVERSAMPLING = 100
};
/*Function prototypes*/
void heartbeat(void);

/*Variables*/
uint16_t frequency = 0;
uint16_t adcValue = 0;

int main(void)
{
	//BSP_IC_init();
	BSP_systick_init();
	BSP_LED_init();
  BSP_ADC_init(NBR_OF_CHANNELS, SAMPLING_FREQUENCY, OVERSAMPLING);
	
	//BSP_PWR_init();
  //BSP_PWR_analogON();

	while(1)
	{
		heartbeat();
	} /* END OF MAIN LOOP*/
} /* END OF MAIN */

void heartbeat(void)
{
			if (clk1000ms == CLK_COMPLETE)
		{
			LED_TOGGLE(LED1);
			clk1000ms = CLK_RESET;
		}
}
